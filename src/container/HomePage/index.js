import React, {Component} from 'react';
import NavigationBar from "../NavigationBar";

class HomePage extends Component {
    render() {
        return (
            <NavigationBar/>
        );
    }
}

export default HomePage;
