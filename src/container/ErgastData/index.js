import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionFormulaErgast from '../../actions/actionFormulaErgast';

import FormulaDriversTable from '../../component/FormulaDriversTable';

class ErgastData extends Component {

    currentYear = null;

    componentWillMount() {
        this.fetchAndUpdate();
    }

    componentDidUpdate() {
        if (this.currentYear !== this.props.ergast.year) {
            this.fetchAndUpdate();
        }
    }

    fetchAndUpdate() {
        this.props.actionFormulaErgast.fetchResults(this.props.ergast.year);
        this.currentYear = this.props.ergast.year;
    }

    render() {
        const results = this.props.ergast.results.resultList || [];
        const winner = this.props.ergast.results.winner || '';
        return (
            <div className="">
                {results.map((item) => {
                    return (
                        <FormulaDriversTable key={item.name} data={item} winnerId={winner} />
                    );
                })}
            </div>
        );
    }
}

ErgastData.propTypes = {
    actionFormulaErgast: PropTypes.object,
    ergast: PropTypes.object
};

let mapStateToProps = (state) => {
    return {
        ergast: state.ergast
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        actionFormulaErgast: bindActionCreators(actionFormulaErgast, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ErgastData);
