import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionFormulaErgast from '../../actions/actionFormulaErgast';

import History from '../../utilities/History';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

class NavigationBar extends Component {

    history = History.getInstance();
    yearRange = [2005, 2015];

    state = {
        activeItem: this.props.ergast.year || null
    };

    createMenuList() {
        let list = [];
        for (let i = this.yearRange[0]; i <= this.yearRange[1]; i++) {
            list.push(
                <Tab key={i} label={i} value={i.toString()} name={i.toString()}/>
            );
        }
        return list;
    };

    handleTabClick = (event) => {
        let year = event.currentTarget.name;
        this.setState({
            activeItem: year
        }, () => {
            this.history.forward('/' + year);
            this.props.actionFormulaErgast.setYear(year);
        });
    };

    render() {
        return (
            <div>
                <img src={'./logo.png'} alt="F1-LOGO" style={{'paddingLeft': '20%'}}/>
                <Paper>
                    <Tabs onChange={this.handleTabClick} value={this.state.activeItem}>
                        {this.createMenuList()}
                    </Tabs>
                </Paper>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        ergast: state.ergast
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        actionFormulaErgast: bindActionCreators(actionFormulaErgast, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);