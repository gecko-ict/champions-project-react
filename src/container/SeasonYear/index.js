import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionFormulaErgast from '../../actions/actionFormulaErgast';

import Navigation from '../NavigationBar';
import ErgastData from '../ErgastData';

class SeasonYear extends Component {

    componentWillMount() {
        this.props.actionFormulaErgast.setYear(this.props.match.params.year);
    }

    render() {
        return (
            <div>
                <Navigation/>
                <ErgastData/>
            </div>
        );
    }
}

SeasonYear.propTypes = {
    actionFormulaErgast: PropTypes.object,
    ergast: PropTypes.object
};

let mapStateToProps = (state) => {
    return {
        ergast: state.ergast
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        actionFormulaErgast: bindActionCreators(actionFormulaErgast, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SeasonYear);
