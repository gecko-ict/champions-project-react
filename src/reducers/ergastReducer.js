import initialState from './initialState';
import { ERGAST_RESULTS, YEAR } from '../actions/actionTypes';

export default (state = initialState.ergast, action) => {
    let newState = {...state};
    switch (action.type) {
        case ERGAST_RESULTS:
            newState.results = action.ergastResults;
            return newState;

        case YEAR:
            newState.year = action.year;
            newState.results = [];
            return newState;

        default:
            return state;
    }
}