import { combineReducers } from 'redux';
import ergast from './ergastReducer';

const rootReducer = combineReducers({
    ergast: ergast
});
export default rootReducer;