import React, {Component} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const winnerRow = {
    backgroundColor: 'green'
};

const regular = {
    backgroundColor: 'light-gray'
};

class FormulaDriversTable extends Component {
    render() {
        return (
            <div style={{"paddingTop":10}}>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Driver</TableCell>
                                <TableCell>No.</TableCell>
                                <TableCell>Position</TableCell>
                                <TableCell>Constructor</TableCell>
                                <TableCell>Points</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.props.data.list.map((item) => {
                             return (
                                  <TableRow key={item.driverId} style={item.driverId === this.props.winnerId ? winnerRow : regular}>
                                     <TableCell>{item.driver}</TableCell>
                                     <TableCell>{item.number}</TableCell>
                                     <TableCell>{item.position}</TableCell>
                                     <TableCell>{item.constructor}</TableCell>
                                     <TableCell>{item.points}</TableCell>
                                 </TableRow>
                                    )
                                })
                            }

                        </TableBody>
                    </Table>
                </Paper>
            </div>
        );
    }
}

export default FormulaDriversTable;
