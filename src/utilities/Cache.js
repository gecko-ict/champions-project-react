class Cache {

    key = 'cache';
    storageName = null;
    data = {};

    constructor(name) {
        if (typeof name !== 'undefined') {
            this.key += name;
        }
        let href = window.location.href;
        href = href.substring(0, href.length - window.location.hash.length);
        this.storageName = this.encode(this.key + href);
        if (sessionStorage.getItem(this.storageName) === null) {
            sessionStorage.setItem(this.storageName, this.encode('{}'));
        }
        else {
            this.data = JSON.parse(this.decode(sessionStorage.getItem(this.storageName)));
        }
    }

    get(key) {
        if (typeof this.data[key] === 'undefined') return null;
        return this.data[key];
    }

    set(key, value) {
        this.data[key] = value;
        this.persist();
        return true;
    }

    delete(key) {
        if (!this.check(key)) return false;
        delete this.data[key];
        this.persist();
        return true;
    }

    check(key) {
        return typeof this.data[key] !== 'undefined';
    }

    dispose() {
        sessionStorage.removeItem(this.storageName);
        this.data = {};
        this.persist();
    }

    persist() {
        let stringified = JSON.stringify(this.data);
        sessionStorage.setItem(this.storageName, this.encode(stringified));
    }

    encode(data) {
        return btoa(data);
    }

    decode(data) {
        return atob(data);
    }
}
export default Cache;
