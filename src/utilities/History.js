import createBrowserHistory from 'history/createBrowserHistory';

class History {

    api = createBrowserHistory({
        basename: '',             // The base URL of the app
        forceRefresh: false,      // Force to full page refreshes
        keyLength: 6,             // The length of location.key
    });

    forward(path) {
        this.api.push(path);
    }

    back() {
        this.api.goBack();
    }
}

let instance = null;
History.getInstance = function () {
    if (instance === null) {
        instance = new History();
    }
    return instance;
};
export default History;
