import axios from 'axios';

class Api {

    baseURL = process.env.REACT_APP_API_URL;
    token = null;

    fetch(method, url, options) {
        let data = options || {};
        data['baseURL'] = this.baseURL;
        data['method'] = method;
        data['url'] = url;
        data['headers'] = data['headers'] || {};
        if (this.token !== null) {
            data['headers']['Authorization'] = this.token;
        }
        return axios(data).then((response) => {
            return response.data;
        }).catch((response) => {
            throw response;
        });
    }
}

let instance = null;
Api.getInstance = () => {
    if (instance === null) {
        instance = new Api();
    }
    return instance;
};

export default Api;
