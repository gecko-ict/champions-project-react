import * as types from './actionTypes';
import ErgastResultsService from '../service/ErgastResultsService';

let ergastResultsService = new ErgastResultsService();

export function fetchResults(year) {
    return dispatch => {
        ergastResultsService.getByYear(year).then((data) => {
            dispatch({type: types.ERGAST_RESULTS, ergastResults: data});
        });
    };
}

export function setYear(year) {
    return dispatch => {
        dispatch({type: types.YEAR, year: year});
    };
}