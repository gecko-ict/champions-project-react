import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import History from './utilities/History';

import { Provider } from 'react-redux';

import HomePage from './container/HomePage';
import SeasonYear from './container/SeasonYear';
import configureStore from "./utilities/reduxStore";

class Root extends Component {
    history = History.getInstance();

    render() {
        const reduxStore = configureStore();
        return (
            <Provider store={reduxStore}>
                    <Router history={this.history.api}>
                        <Switch>
                            <Route exact path="/" component={HomePage} />
                            <Route exact path="/:year" component={SeasonYear} />
                        </Switch>
                    </Router>
            </Provider>
        );
    }
}

export default Root;