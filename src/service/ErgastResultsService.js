import Api from '../utilities/Api';
import Cache from '../utilities/Cache';

class ErgastResultsService {

    api = Api.getInstance();
    cache = new Cache('ergast-results');


    getByYear(year) {
        if (this.cache.check(year)) {
            return new Promise((resolve) => {
                resolve(this.cache.get(year));
            });
        }

        return this.api.fetch('GET', `api/f1/${year}/results.json?limit=500&offset=0`).then((results) => {
            let resultList = [];
            let data = results.MRData.RaceTable.Races;
            let winnningMap = {};
            for (let i = 0; i < data.length; i++) {
                let race = {
                    name: data[i].raceName,
                    list: []
                };
                for (let j = 0; j < data[i].Results.length; j++) {
                    race.list.push({
                        position: data[i].Results[j].position,
                        number: data[i].Results[j].number,
                        driver: data[i].Results[j].Driver.givenName + ' ' + data[i].Results[j].Driver.familyName,
                        constructor: data[i].Results[j].Constructor.name,
                        points: data[i].Results[j].points,
                        driverId: data[i].Results[j].Driver.driverId
                    });
                    if (typeof winnningMap[data[i].Results[j].Driver.driverId] === 'undefined') {
                        winnningMap[data[i].Results[j].Driver.driverId] = parseInt(data[i].Results[j].points, 10)
                    } else {
                        winnningMap[data[i].Results[j].Driver.driverId] += parseInt(data[i].Results[j].points, 10)
                    }
                }
                resultList.push(race)
            }
            let top = 0;
            let winner = null;
            for (let prop in winnningMap) {
                if (winnningMap[prop] > top) {
                    top = winnningMap[prop];
                    winner = prop
                }
            }
            this.cache.set(year, { resultList, winner });
            return { resultList, winner };
        });
    }
}
export default ErgastResultsService;