# Formula 1 Champions React Application

Single page web application for F1 world champions that were from 2005 to 2015.

## Getting started
 - `git clone https://gitlab.com/gecko-ict/champions-project-react.git
 - `cd champions-project-react`

## Run in development mode
 - `npm install`
 - `npm start`

## Build for production
 - `npm install`
 - `npm run build`

## Architecture
 - Layered architecture
 - Reusable components

## Project structure (src directory)
 - actions/ - Redux actions
 - component/ - Simple components
 - container/ - Redux components
 - reducers/ - Redux reducers
 - service/ - Business logic layer
 - utilites/ - Util classes

## Technologies
 - [React](https://reactjs.org/)
 - [Redux](https://redux.js.org/)
 - [Material UI](https://material-ui.com/)
 - [Axios](https://github.com/axios/axios)
 - [Webpack](https://webpack.js.org/)